var app = angular.module("appTracking", [ "angular-loading-bar", "file-model", "ngDialog", "ngResource", "ngRoute", "ngSanitize", "ngStorage", "oitozero.ngSweetAlert", "omr.directives", "simplePagination" ])

.config(["$routeProvider", "$httpProvider", "cfpLoadingBarProvider",
function ($routeProvider, $httpProvider, cfpLoadingBarProvider)
{
	$routeProvider
	.when("/areasoferta",
	{
		controller: "AreaOfertaController",
		listview: true,
		templateUrl: "module/areasoferta",
		title: "Catálogo de Areas de oferta"
	})
		.when("/areasoferta/new",
		{
			controller: "AreaOfertaController",
			newform: true,
			templateUrl: "form/areaoferta",
			title: "Nueva Area de Oferta"
		})
	.when("/candidatos",
	{
		controller: "CandidatoController",
		listview: true,
		templateUrl: "module/candidatos",
		title: "Candidatos"
	})
		.when("/candidatos/new",
		{
			controller: "CandidatoController",
			newform: true,
			templateUrl: "form/candidato",
			title: "Nuevo Candidato"
		})
		.when("/candidatos/:id",
		{
			controller: "CandidatoController",
			editform: true,
			templateUrl: "article/candidato",
			title: "Candidato"
		})
	.when("/empleos",
	{
		controller: "EmpleoController",
		listview: true,
		templateUrl: "module/empleos",
		title: "Empleos"
	})
		.when("/empleos/new",
		{
			controller: "EmpleoController",
			newform: true,
			templateUrl: "form/empleo",
			title: "Nueva Empleo"
		})
		.when("/empleos/:id",
		{
			controller: "EmpleoController",
			editform: true,
			templateUrl: "article/empleo",
			title: "Empleos"
		})
	.when("/empresas",
	{
		controller: "EmpresaController",
		listview: true,
		templateUrl: "module/empresas",
		title: "Empresas"
	})
		.when("/empresas/new",
		{
			controller: "EmpresaController",
			newform: true,
			templateUrl: "form/empresa",
			title: "Nueva Empresa"
		})
		.when("/empresas/:id",
		{
			controller: "EmpresaController",
			editform: true,
			templateUrl: "article/empresa",
			title: "Empresa"
		})
	.when("/instituciones",
	{
		controller: "InstitucionController",
		listview: true,
		templateUrl: "module/instituciones",
		title: "Catálogo de Instituciones"
	})
		.when("/instituciones/new",
		{
			controller: "InstitucionController",
			newform: true,
			templateUrl: "form/institucion",
			title: "Nueva Institucion"
		})
		.when("/instituciones/:id",
		{
			controller: "InstitucionController",
			editform: true,
			templateUrl: "module/instituciones",
			title: "Editar unidad"
		})
	.when("/habilidades",
	{
		controller: "HabilidadController",
		listview: true,
		templateUrl: "module/habilidades",
		title: "Catálogo de Habilidades"
	})
		.when("/habilidades/new",
		{
			controller: "HabilidadController",
			newform: true,
			templateUrl: "form/habilidad",
			title: "Nueva Habilidad"
		})
	.when("/home",
	{
		controller: "HomeController",
		templateUrl: "module/dashboard",
		title: "Inicio"
	})
	.when("/logout",
	{
		controller: "LogoutController",
		templateUrl: "module/logout",
		title: "...Saliendo..."
	})
	.when("/profesiones",
	{
		controller: "ProfesionController",
		listview: true,
		templateUrl: "module/profesiones",
		title: "Catalogo de Profesiones"
	})
		.when("/profesiones/new",
		{
			controller: "ProfesionController",
			newform: true,
			templateUrl: "form/profesion",
			title: "Nueva Profesion"
		})
		.when("/profesiones/:id",
		{
			controller: "ProfesionController",
			editform: true,
			templateUrl: "module/profesiones",
			title: "Editar profesion"
		})
	.when("/reportes",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportes",
		title: "Mis Reportes"
	})
	.when("/reportes/ofertas",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportesofertas",
		title: "Mis Reportes de Ofertas"
	})
	.when("/reportes/candidatos",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportescandidatos",
		title: "Mis Reportes de Candidatos"
	})
	.when("/reportes",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportes",
		title: "Mis Reportes"
	})
	.when("/tiposempresa",
	{
		controller: "TipoEmpresaController",
		listview: true,
		templateUrl: "module/tiposempresa",
		title: "Catálogo de Tipos de Empresa"
	})
		.when("/tiposempresa/new",
		{
			controller: "TipoEmpresaController",
			newform: true,
			templateUrl: "form/tipoempresa",
			title: "Nuevo Tipo de empresa"
		})
	.otherwise({ redirectTo: "/home" });

	$httpProvider.interceptors.push(["$q", "$location", "$localStorage", "SweetAlert", function ($q, $location, $localStorage, SweetAlert) 
	{
		return {
			"request": function (config)
			{
				console.log( "Data: " );
				console.log( config.data  );

				config.headers = config.headers || {};
				if ($localStorage.token)
				{
					config.headers.Authorization = "Bearer " + $localStorage.token;
				}
				
				return config;
			},
			"responseError": function (response)
			{
				if( response.status === 400 )
				{
					var errors = null;

					if( response.data.errors != undefined )
						errors = Object.keys( response.data.errors )
									.toString()
									.replace(/id_/g, "")
									.replace(/_/g, " ");

					SweetAlert.swal("Error al guardar", "Revisa los siguientes campos: " + errors, "error");
				}
				else if (response.status === 401 || response.status === 403)
				{
					$location.path("/logout");
				}
				else if( response.status === -1 )
				{
					SweetAlert.swal("Sin conexión", "No es posible establecer conexión con el servidor", "error");
				}
				else
				{
					SweetAlert.swal("Error al guardar", "Revisa que no falte ningun dato importante y prueba de nuevo", "error");
				}

				console.log( response );

				return $q.reject(response);
			}
		};
	}]);

	$httpProvider.defaults.transformRequest = function(data)
	{
		if (data === undefined)
			return data;

		var fd = new FormData();
		angular.forEach(data, function(value, key)
		{
			if (value instanceof FileList)
			{
				if (value.length == 1)
				{
					fd.append(key, value[0]);
				}
				else
				{
					angular.forEach(value,
						function(file, index) 
						{
							fd.append(key + "_" + index, file);
						});
				}
			}
			else
			{
				fd.append(key, value);
			}
		});

		return fd;
	}

	$httpProvider.defaults.headers.post["Content-Type"] = undefined;

	cfpLoadingBarProvider.includeSpinner = false;
}])

.constant("urls",
{
	BASE: "/",
	BASE_API: "/api",
	BASE_REPORT: "/report",
	APP_TITLE: " PeopleWork :: Customer Relationship Management "
})

.controller("AreaOfertaController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "AreaOferta",
function($rootScope, $scope, $location, $routeParams, SweetAlert, AreaOferta)
{
	$scope.current_date = new Date();
	$scope.areaoferta = {};

	if( $scope.listview )
	{
		AreaOferta.get(function(data)
		{
			$scope.areasoferta = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				AreaOferta.save( $scope.areaoferta, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Area de oferta creada", "success");
					$location.path( "areasoferta" );
				});
			}

			else if( $scope.editform )
			{
				AreaOferta.update( $scope.areaoferta, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Area de oferta modificada", "success");
					$location.path( "areasoferta" );
				});
			}
		}
}])

.controller("CandidatoController", ["$rootScope", "$scope", "$filter", "$location", "$routeParams", "SweetAlert", "Candidato", "CandidatoHabilidad", "Departamento", "Habilidad", "Municipio",
function($rootScope, $scope, $filter, $location, $routeParams, SweetAlert, Candidato, CandidatoHabilidad, Departamento, Habilidad, Municipio)
{
	$scope.current_date = new Date();
	$scope.candidato = {};

	if( $scope.listview )
	{
		Candidato.get(function(data)
		{
			$scope.candidatos = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
		$scope.candidatohabilidad = { idcandidato: id };

		Candidato.get({id:id}, function(data)
		{
			$scope.candidato = data.record;
		});

		CandidatoHabilidad.get(function(data)
		{
			$scope.candidatohabilidades = data.records;
		});

		Departamento.get(function(data)
		{
			$scope.departamentos = data.records;
		});

		Habilidad.get(function(data)
		{
			$scope.habilidades = data.records;
		});

		Municipio.get(function(data)
		{
			$scope.municipios = data.records;
		});
	}

	else
	{
		Departamento.get(function(data)
		{
			$scope.departamentos = data.records;
		});

		Municipio.get(function(data)
		{
			$scope.municipios = data.records;
		});
	}

	$scope.addHabilidad = function()
	{
		CandidatoHabilidad.save( $scope.candidatohabilidad, function(data)
		{
			SweetAlert.swal("Operación exitosa", "Habilidad Agregada", "success");

			CandidatoHabilidad.get(function(data)
			{
				$scope.candidatohabilidades = data.records;
			});
		});
	}

	$scope.submit = function()
		{
			var formatFechaNacimiento = $filter("date")($scope.candidato.fechanacimiento, "yyyy-MM-dd");
			$scope.candidato.fechanacimiento = formatFechaNacimiento;

			if( $scope.newform )
			{	
				Candidato.save( $scope.candidato, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion creada", "success");
					$location.path( "candidatos" );
				});
			}

			else if( $scope.editform )
			{
				Candidato.update( $scope.candidato, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion modificada", "success");
					$location.path( "candidatos" );
				});
			}
		}
}])

.controller("HabilidadController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Habilidad",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Habilidad)
{
	$scope.current_date = new Date();
	$scope.habilidad = {};

	if( $scope.listview )
	{
		Habilidad.get(function(data)
		{
			$scope.habilidades = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Habilidad.save( $scope.habilidad, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Habilidad creada", "success");
					$location.path( "habilidades" );
				});
			}

			else if( $scope.editform )
			{
				Habilidad.update( $scope.habilidad, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Habilidad modificada", "success");
					$location.path( "habilidades" );
				});
			}
		}
}])

.controller("EmpleoController", ["$rootScope", "$scope", "$filter", "$location", "$routeParams", "SweetAlert", "Departamento", "Empleo", "Empresa", "Jornada", "Municipio",
function($rootScope, $scope, $filter, $location, $routeParams, SweetAlert, Departamento, Empleo, Empresa, Jornada, Municipio)
{
	$scope.current_date = new Date();
	$scope.empleo = {};

	if( $scope.listview )
	{
		Empleo.get(function(data)
		{
			$scope.empleos = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Empleo.get({id:id}, function(data)
		{
			$scope.empleo = data.record;
		});
	}

	else
	{
		Departamento.get(function(data)
		{
			$scope.departamentos = data.records;
		});

		Empresa.get(function(data)
		{
			$scope.empresas = data.records;
		});

		Jornada.get(function(data)
		{
			$scope.jornadas = data.records;
		});

		Municipio.get(function(data)
		{
			$scope.municipios = data.records;
		});
	}

	$scope.submit = function()
		{
			var formatFechaContratacion = $filter("date")($scope.empleo.fechacontratacion, "yyyy-MM-dd");
			var formatFechaPostulacion = $filter("date")($scope.empleo.fechapostulacion, "yyyy-MM-dd");
			$scope.empleo.fechacontratacion = formatFechaContratacion;
			$scope.empleo.fechapostulacion = formatFechaPostulacion;

			if( $scope.newform )
			{
				Empleo.save( $scope.empleo, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Empleo creado", "success");
					$location.path( "empleos" );
				});
			}

			else if( $scope.editform )
			{
				Empleo.update( $scope.empleo, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Empleo modificado", "success");
					$location.path( "empleos" );
				});
			}
		}
}])

.controller("EmpresaController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Empresa", "EmpresaSector", "Sector",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Empresa, EmpresaSector, Sector)
{
	$scope.current_date = new Date();
	$scope.empresa = {};

	if( $scope.listview )
	{
		Empresa.get(function(data)
		{
			$scope.empresas = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
		$scope.empresasector = { idempresa: id };

		Empresa.get({id:id}, function(data)
		{
			$scope.empresa = data.record;
		});

		EmpresaSector.get(function(data)
		{
			$scope.empresasectores = data.records;
		})

		Sector.get(function(data)
		{
			$scope.sectores = data.records;
		});
	}

	$scope.addSector = function()
	{
		EmpresaSector.save( $scope.empresasector, function(data)
		{
			SweetAlert.swal("Operación exitosa", "Sector Agregado", "success");

			EmpresaSector.get(function(data)
			{
				$scope.empresasectores = data.records;
			})
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Empresa.save( $scope.empresa, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion creada", "success");
					$location.path( "empresas" );
				});
			}

			else if( $scope.editform )
			{
				Empresa.update( $scope.empresa, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion modificada", "success");
					$location.path( "empresas" );
				});
			}
		}
}])

.controller("InstitucionController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "InstitucionEducativa",
function($rootScope, $scope, $location, $routeParams, SweetAlert, InstitucionEducativa)
{
	$scope.current_date = new Date();
	$scope.institucion = {};

	if( $scope.listview )
	{
		InstitucionEducativa.get(function(data)
		{
			$scope.instituciones = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				InstitucionEducativa.save( $scope.institucion, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion creada", "success");
					$location.path( "instituciones" );
				});
			}

			else if( $scope.editform )
			{
				InstitucionEducativa.update( $scope.institucion, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Institucion modificada", "success");
					$location.path( "instituciones" );
				});
			}
		}
}])

.controller("HeaderController", ["$rootScope", "$scope", "$location", "$window", "Auth",
function($rootScope, $scope, $location, $window, Auth)
{
	$scope.current_date 		= new Date();
}])

.controller("HomeController", ["$rootScope", "$scope",
function($rootScope, $scope, Documento)
{
	$rootScope.currentModule = "home";
	$scope.current_date = new Date();
}])

.controller("LogoutController", ["$rootScope", "$scope", "$window", "$localStorage", "Auth",
function($rootScope, $scope, $window, $localStorage, Auth)
{
	Auth.logout(function()
	{
		$window.location.href = "login";
	});
}])

.controller("ProfesionController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Profesion",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Profesion)
{
	$scope.current_date = new Date();
	$scope.profesion = {};

	if( $scope.listview )
	{
		Profesion.get(function(data)
		{
			$scope.profesiones = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Profesion.save( $scope.profesion, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Profesion creada", "success");
					$location.path( "profesiones" );
				});
			}

			else if( $scope.editform )
			{
				Profesion.update( $scope.profesion, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Profesion modificada", "success");
					$location.path( "profesiones" );
				});
			}
		}
}])

.controller("ReporteController", ["$rootScope", "$scope", "$filter", "$window", "$httpParamSerializer", "urls", "Candidato", "Departamento", "Empleo", "Jornada", "Municipio",
function($rootScope, $scope, $filter, $window, $httpParamSerializer, urls, Candidato, Departamento, Empleo, Jornada, Municipio)
{
	$rootScope.currentModule = "reportes";
	$scope.current_date = new Date();

	Departamento.get(function(data)
	{
		$scope.departamentos = data.records;
	});

	Jornada.get(function(data)
	{
		$scope.jornadas = data.records;
	});

	Municipio.get(function(data)
	{
		$scope.municipios = data.records;
	});

	$scope.generateReportCandidatos = function()
	{
		Candidato.get($scope.reportecandidato, function(data)
		{
			$scope.candidatos = data.records;
		})
	}

	$scope.generateReportOfertas = function()
	{
		var formatFechaPostulacion = $filter("date")($scope.reporteoferta.fechapostulacion, "yyyy-MM-dd");
		$scope.reporteoferta.fechapostulacion = formatFechaPostulacion;
		
		Empleo.get($scope.reporteoferta, function(data)
		{
			$scope.empleos = data.records;
		})
	}

	$scope.generateReportOperaciones = function()
		{
			var params = $httpParamSerializer( $scope.reporte_operaciones );
			$window.open( urls.BASE_REPORT + "/operaciones?" + params );
		}
}])

.controller("TipoEmpresaController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "TipoEmpresa",
function($rootScope, $scope, $location, $routeParams, SweetAlert, TipoEmpresa)
{
	$scope.current_date = new Date();
	$scope.tipoempresa = {};

	if( $scope.listview )
	{
		TipoEmpresa.get(function(data)
		{
			$scope.tiposempresa = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		// Unidad.get({id:id}, function(data)
		// {
		// 	$scope.unidad = data.record;
		// });
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				TipoEmpresa.save( $scope.tipoempresa, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Tipo empresa creada", "success");
					$location.path( "tiposempresa" );
				});
			}

			else if( $scope.editform )
			{
				TipoEmpresa.update( $scope.tipoempresa, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Tipo empresa modificada", "success");
					$location.path( "tiposempresa" );
				});
			}
		}
}])

.directive("apiSrc", ["$http", "$localStorage", function ($http, $localStorage)
{
	var directive =
	{
		link: link,
		restrict: "A"
	};
	return directive;

	function link(scope, element, attrs)
	{
		var requestConfig =
		{
			method: "GET",
			url: attrs.apiSrc,
			responseType: "arraybuffer",
			cache: "true",
			headers:
			{
				"Authorization": "Bearer " + $localStorage.token
			}
		};

		$http(requestConfig).success(function(data)
		{
			var arr = new Uint8Array(data);

			var raw = "";
			var i, j, subArray, chunk = 5000;
			for (i = 0, j = arr.length; i < j; i += chunk)
			{
				subArray = arr.subarray(i, i + chunk);
				raw += String.fromCharCode.apply(null, subArray);
			}

			var b64 = btoa(raw);
			attrs.$set("src", "data:image/jpeg;base64," + b64);
		});
	}
}])

.directive("stringToNumber", function()
{
	return {
		require: "ngModel",
		link: 
			function(scope, element, attrs, ngModel)
			{
				ngModel.$parsers.push(
					function(value)
					{
						return "" + value;
					}
				);
			
				ngModel.$formatters.push(
					function(value)
					{
						return parseFloat(value, 10);
					}
				);
			}
	}
})

.factory("Auth", ["$http", "$localStorage", "urls", function ($http, $localStorage, urls)
{
	function urlBase64Decode(str)
	{
		var output = str.replace("-", "+").replace("_", "/");
		switch (output.length % 4)
		{
			case 0:
				break;
			case 2:
				output += "==";
				break;
			case 3:
				output += "=";
				break;
			default:
				throw "Illegal base64url string!";
		}

		return window.atob(output);
	}

	function getClaimsFromToken()
	{
		var token = $localStorage.token;
		var user = {};
		if (typeof token !== "undefined")
		{
			var encoded = token.split(".")[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}

		return user;
	}

	var tokenClaims = getClaimsFromToken();

	return {
		signup: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		signin: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		logout: function (success)
		{
			tokenClaims = {};
			delete $localStorage.token;
			success();
		},
		getTokenClaims: function ()
		{
			return tokenClaims;
		},
		getAuthenticatedUser: function(success)
		{
			$http.get(urls.BASE_API + "/authenticate").success(success);
		}
	};
}])

.factory("AreaOferta", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/areaoferta");
}])

.factory("Asunto", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/asunto");
}])

.factory("Candidato", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/candidato/:id");
}])

.factory("CandidatoHabilidad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/candidatohabilidad/:id");
}])

.factory("Departamento", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/departamento");
}])

.factory("Empresa", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/empresa/:id",
						{ 
							"id": "@id",
							"nombreempresa": "@nombreempresa",
							"descripcionempresa": "@descripcionempresa",
							"email": "@email",
							"direccionempresa": "@direccionempresa",
							"nitempresa": "@nitempresa",
							"websiteempresa": "@websiteempresa"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("EmpresaSector", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/empresasector/:id");
}])

.factory("Empleo", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/empleo/:id");
}])

.factory("Habilidad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/habilidad/:id");
}])

.factory("InstitucionEducativa", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/institucioneducativa/:id");
}])

.factory("Jornada", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/jornada");
}])

.factory("Municipio", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/municipio");
}])

.factory("Pais", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/pais");
}])

.factory("Password", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/password");
}])

.factory("Profesion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/profesion");
}])

.factory("Puesto", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/puesto/:id", 
						{ 
							"id": "@id",
							"director": "@director",
							"monitor": "@monitor",
							"nombre": "@nombre"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Reporte", ["$resource", "urls", function($http, urls)
{
	return {
		documento: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/documento", data).success(success).error(error)
		},
		expedientes: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/expedientes", data).success(success).error(error)
		},
		operaciones: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/operaciones", data).success(success).error(error)
		}
	};
}])

.factory("Sector", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/sector");
}])

.factory("TempPersona", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/temppersona", { nombre: "@nombre", numero_dpi: "@numero_dpi" });
}])

.factory("TipoEmpresa", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/tipoempresa/:id");
}])

.factory("Unidad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/unidad/:id", 
						{ 
							"id": "@id",
							"id_director": "@id_director",
							"id_gerente": "@id_gerente",
							"id_institucion": "@id_institucion",
							"id_supervisor": "@id_supervisor",
							"nombre": "@nombre",
							"siglas": "@siglas"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Usuario", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/usuario/:id", { id: "@id" });
}])

.filter("abs", function ()
{
	return function(val) { return Math.abs(val); }
})

.filter("dotnotation", function ()
{
	return function(arr, property) { 
		var obj 		= {};

		for( item in arr ) // Loop the objects of the array
		{
			for( key in arr[item] ) // Loop the keys of the object
			{
				var num = parseInt(item) + 1;
				obj[ property + "." + num + "." + key ] = arr[item][key];
			}
		}

		return obj;
	}
})

.filter("go", ["$location", function ($location)
{
	return function(val) { $location.path( val ); }
}])

.filter("pad", function ()
{
	return function( num, size ) {
		var s = num + "";
		while (s.length < size) s = "0" + s;
		return s;
	};
})

.run(["$location", "$rootScope", "urls", function($location, $rootScope, urls)
{
	$rootScope.$on("$routeChangeSuccess", function (event, current, previous)
	{
		if( current.$$route !== undefined )
		{
			$rootScope.article 	= current.$$route.article;
			$rootScope.editform = current.$$route.editform;
			$rootScope.listview = current.$$route.listview;
			$rootScope.newform 	= current.$$route.newform;
			$rootScope.title 	= current.$$route.title + urls.APP_TITLE;
			$rootScope.showNotificacionDropdown = false;
			$rootScope.showUserDropdown 		= false;
		}
	});
}]);