<section class="content-block">
	<header>
		<h2>Fuentes de Financiamiento</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="fuenteFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'recursoshumanos/fuentesfinanciamiento/new' | go">
				<span class="icon-add_circle"></span>
				Agregar nueva fuente de financiamiento
			</button>
		</div>
		<div class="item" ng-repeat="fuente in fuentesfinanciamiento | filter:fuenteFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage | orderBy:created_at:true" 
			ng-click="'recursoshumanos/fuentesfinanciamiento/'+fuente.id | go">
			<label class="subject">
				{{fuente.nombre}}
			</label>
			<div>
				<strong>Descripción:</strong>
				{{fuente.descripcion}}
			</div>
		</div>
		<div class="pagination" ng-if="fuentesfinanciamiento.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="fuentesfinanciamiento.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>