<section class="content-block">
	<header>
		<h2>Usuarios del sistema</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="usuarioFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
				<div class="input-options">
					<div class="select">
						Filtrar por unidad:
						<select ng-model="usuarioFiltro.id_unidad">
							<option value="">--- TODOS ---</option>
							<option ng-repeat="unidad in unidades" value="{{unidad.id}}">
								{{unidad.nombre}}
							</option>
						</select>
					</div>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item" ng-if="rrhh != true">
			<button ng-click="'usuarios/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo usuario
			</button>
		</div>
		<div class="item add-item" ng-if="rrhh == true">
			<button ng-click="'recursoshumanos/new' | go">
				<span class="icon-add_circle"></span>
				Registrar nueva persona
			</button>
		</div>
		<div class="item" ng-repeat="usuario in usuarios | filter:usuarioFiltro | orderBy:usuario.id_unidad | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage" ng-click="'usuarios/'+usuario.id | go">
			<label class="subject">
				{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}
				<span class="priority-tag ontime">{{usuario.unidad.nombre}}</span>
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Nombre de usuario:</strong>
				{{usuario.usuario}}
				<br/>
				<span class="icon-work"></span>
				<strong>Puesto:</strong>
				{{usuario.puesto.nombre}}
				<br/>
				<span class="icon-today"></span>
				{{usuario.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="pagination" ng-if="usuarios.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="usuarios.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>