<section class="content-block">
	<header>
		<h2>Puestos del sistema</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" class="min" ng-model="puestoFiltro" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'puestos/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo puesto
			</button>
		</div>
		<div class="item" ng-repeat="puesto in puestos | filter:puestoFiltro" ng-click="'puestos/'+puesto.id | go">
			<label class="subject">
				{{puesto.nombre}}
			</label>
		</div>
		<div class="item not-found" ng-if="puestos.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>