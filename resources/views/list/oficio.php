<section class="content-block">
	<header>
		<h2>Oficios pendientes de revisar</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="oficioFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
				<div class="input-options">
					<div class="select">
						Filtrar por estado:
						<select ng-model="oficioFiltro.id_estado">
							<option value="">--- TODOS ---</option>
							<option ng-repeat="estado in estados" value="{{estado.id}}">
								{{estado.nombre}}
							</option>
						</select>
						<label ng-show="oficioFiltro.id_estado==1">
							<input type="checkbox" ng-model="oficioLeido" value="1" >
							Mostrar Leídos
						</label>
					</div>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'oficios/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo oficio
			</button>
		</div>
		<div class="item" ng-repeat="oficio in oficios | filter:oficioFiltro"
						ng-if="oficio.id_estado != 1 || ( oficio.id_estado == 1 && oficio.fecha_recibido != '0000-00-00 00:00:00' && oficioLeido )"
						ng-click="'oficios/'+oficio.id | go">
			<label class="priority-tag days square left inhurry" 
					ng-class="{ ontime: oficio.dias_diferencia_respuesta>2, inhurry: oficio.dias_diferencia_respuesta>0, outtime: oficio.dias_diferencia_respuesta<1 }"
					ng-if="oficio.id_estado < 3">
				<span>
					{{ ( oficio.dias_diferencia_respuesta = oficio.dias_respuesta - ( current_date | amDifference : oficio.created_at : 'days' ) ) | abs }}
				</span>
			</label>
			<label class="subject">
				<span ng-class="{ 'icon-attach_file': oficio.src_file.length > 0 }"></span>
				{{oficio.asunto.nombre}}
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Creado por:</strong>
				{{oficio.usuario_creo.apellido_1}} {{oficio.usuario_creo.apellido_2}}, {{oficio.usuario_creo.nombre_1}} {{oficio.usuario_creo.nombre_2}}
				<br/>
				<span class="icon-today"></span>
				{{oficio.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="item not-found" ng-if="oficios.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>