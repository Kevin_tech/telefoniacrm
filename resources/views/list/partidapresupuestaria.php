<section class="content-block">
	<header>
		<h2>Partidas Presupuestarias</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="partidaFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'recursoshumanos/partidaspresupuestarias/new' | go">
				<span class="icon-add_circle"></span>
				Agregar nueva partida presupuestaria
			</button>
		</div>
		<div class="item" ng-repeat="partida in partidaspresupuestarias | filter:partidaFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage | orderBy:created_at:true" 
			ng-click="'recursoshumanos/partidaspresupuestarias/'+partida.id | go">
			<label class="subject">
				{{partida.actividad}} -
				{{partida.ejercicio_fiscal}} -
				{{partida.entidad}} -
				{{partida.obra}} -
				{{partida.programa}} -
				{{partida.proyecto}} -
				{{partida.sub_programa}} -
				{{partida.ubicacion}} -
				{{partida.unidad_desconcentrada}} -
				{{partida.unidad_ejecutora}}
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Fuente de Financiamiento:</strong>
				{{partida.fuente_financiamiento.nombre}}
				<br/>
				<span class="icon-work"></span>
				<strong>Renglon:</strong>
				{{partida.renglon.nombre}}
			</div>
		</div>
		<div class="pagination" ng-if="partidaspresupuestarias.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="partidaspresupuestarias.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>