<section class="content-block">
	<header>
		<h2>Mis Procedimientos</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="procedimientoFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'procedimientos/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo procedimiento
			</button>
		</div>
		<div class="item" ng-repeat="procedimiento in procedimientos | filter:procedimientoFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage" 
			ng-click="'procedimientos/'+procedimiento.id | go">
			<label class="subject">
				{{procedimiento.titulo}}
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Creado por:</strong>
				{{procedimiento.usuario.apellido_1}} {{procedimiento.usuario.apellido_2}}, {{procedimiento.usuario.nombre_1}} {{procedimiento.usuario.nombre_2}}
				<br/>
				<span class="icon-today"></span>
				{{procedimiento.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="pagination" ng-if="procedimientos.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="procedimientos.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>