<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Ingresa los datos para el nuevo candidato</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="candidato.nombres" placeholder="Nombres" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="candidato.apellidos" placeholder="Apellidos" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="candidato.email" placeholder="E-Mail" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="candidato.telpersonal" placeholder="Teléfono Móvil" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="candidato.telcasa" placeholder="Teléfono Fijo" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="candidato.direccionresidencia" placeholder="Dirección" required />
		</div>
		<div class="col-xs-3">
			<select ng-model="candidato.iddepartamento">
				<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">
					{{departamento.descripciondepartamento}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<select ng-model="candidato.idmunicipio">
				<option ng-repeat="municipio in municipios | filter : { iddepartamento: candidato.iddepartamento }" value="{{municipio.id}}">
				{{municipio.descripcionmunicipio}}
			</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="date" class="form-control" ng-model="candidato.fechanacimiento" placeholder="Fecha de Nacimiento" required />
		</div>
		<div class="col-xs-3">
			<select ng-model="candidato.genero">
				<option value="0">Seleccione el Género</option>
				<option value="1">Hombre</option>
				<option value="2">Mujer</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>