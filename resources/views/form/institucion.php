<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Ingresa los datos para la nueva institución educativa</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="institucion.descinstitucionedu" placeholder="Descripcion" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="institucion.direccion" placeholder="Direccion" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="institucion.encargado" placeholder="Nombre del encargado" required />
		</div>
		<div class="col-xs-3">
			<input type="number" class="form-control" ng-model="institucion.telefono1" placeholder="Número de Teléfono 1" required />
		</div>
		<div class="col-xs-3">
			<input type="number" class="form-control" ng-model="institucion.telefono2" placeholder="Número de Teléfono 2" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>