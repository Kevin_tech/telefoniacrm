<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Ingresa los datos para la nueva empresa</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.nombreempresa" placeholder="Nombre de la empresa" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="empresa.descripcionempresa" placeholder="Descripción" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.email" placeholder="E-Mail" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="empresa.direccionempresa" placeholder="Dirección" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="empresa.nitempresa" placeholder="N.I.T." required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.websiteempresa" placeholder="Sitio Web" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>