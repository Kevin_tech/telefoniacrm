<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">Catálogo de Habilidades</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Descripción de la Habilidad</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="habilidad in habilidades">
					<td>{{habilidad.deschabilidad}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/habilidades/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>