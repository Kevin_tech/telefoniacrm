<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">Catálogo de Instituciones Educativas</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre de la institución</th>
					<th class="text-turquoise">Dirección</th>
					<th class="text-turquoise">Encargado</th>
					<th class="text-turquoise">Teléfono</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="institucion in instituciones">
					<td>{{institucion.descinstitucionedu}}</td>
					<td>{{institucion.direccion}}</td>
					<td>{{institucion.encargado}}</td>
					<td>
						{{institucion.telefono1}},
						{{institucion.telefono2}}
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/instituciones/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>