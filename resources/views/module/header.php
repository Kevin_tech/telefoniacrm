<div class="row demo-row">
  <div class="col-xs-2 col-xs-offset-1">
    <img src="img/logo.png" width="100%">
  </div>
  <div class="col-xs-8">
    <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
          <span class="sr-only">Toggle navigation</span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse-01">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="#/home">Inicio</a></li>
          <li><a href="#/empresas">Empresas</a></li>
          <li><a href="#/candidatos">Candidatos</a></li>
          <li><a href="#/empleos">Ofertas de Trabajo</a></li>
          <!-- <li><a href="#/reportes">Reportes</a></li> -->
          <li class="dropdown">
            <a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown">Reportes <b class="caret"></b></a>
            <span class="dropdown-arrow"></span>
            <ul class="dropdown-menu">
              <li><a href="#/reportes/ofertas">Ofertas de trabajo</a></li>
              <li><a href="#/reportes/candidatos">Candidatos por Habilidad</a></li>
              <li class="divider"></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown">Catálogos <b class="caret"></b></a>
            <span class="dropdown-arrow"></span>
            <ul class="dropdown-menu">
              <li><a href="#/habilidades">Habilidades</a></li>
              <li><a href="#/areasoferta">Áreas de Oferta</a></li>
              <li><a href="#/tiposempresa">Tipos de Empresa</a></li>
              <li><a href="#/instituciones">Instituciones Educativas</a></li>
              <li><a href="#/profesiones">Profesiones</a></li>
              <li class="divider"></li>
            </ul>
          </li>
          </ul>
      </div>
    </nav>
  </div>
</div>