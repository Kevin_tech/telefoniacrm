<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">Empleos disponibles</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Empleo</th>
					<th class="text-turquoise">Empresa</th>
					<th class="text-turquoise">Requerimientos</th>
					<th class="text-turquoise">Número de Vacantes</th>
					<th class="text-turquoise">Jornada</th>
					<th class="text-turquoise">Municipio</th>
					<th class="text-turquoise">Sueldo</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="empleo in empleos" 
				ng-click="'empleos/'+empleo.id | go">
					<td>{{empleo.nombreempleo}}</td>
					<td>{{empleo.empresa.nombreempresa}}</td>
					<td>{{empleo.requerimientos}}</td>
					<td>{{empleo.numerovacantes}}</td>
					<td>{{empleo.jornada.nombrejornada}}</td>
					<td>{{empleo.municipio.descripcionmunicipio}}</td>
					<td>Q. {{empleo.sueldo}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/empleos/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>