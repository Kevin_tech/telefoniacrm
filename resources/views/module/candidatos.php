<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">Candidatos registrados</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombres</th>
					<th class="text-turquoise">Direccion</th>
					<th class="text-turquoise">E-Mail</th>
					<th class="text-turquoise">Teléfonos</th>
					<th class="text-turquoise">Fecha de Nacimiento</th>
					<th class="text-turquoise">Municipio</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="candidato in candidatos" 
				ng-click="'candidatos/'+candidato.id | go">
					<td>{{candidato.nombres}} {{candidato.apellidos}}</td>
					<td>{{candidato.direccionresidencia}}</td>
					<td>{{candidato.email}}</td>
					<td>{{candidato.telpersonal}} {{candidato.telcasa}}</td>
					<td>{{candidato.fechanacimiento}}</td>
					<td>{{candidato.idmunicipio}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-3 col-xs-offset-1 keypad">
		<a href="#/candidatos/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Candidato
		</a>
	</div>
</div>