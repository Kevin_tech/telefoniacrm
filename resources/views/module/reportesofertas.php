<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Reporte de Ofertas de Trabajo</h4>
		<span>Puedes utilizar los filtros para obtener mejores resultados</span>
	</div>
</div>

<form enctype="multipart/form-data" ng-submit="generateReportOfertas()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<span>Jornada</span>
			<select ng-model="reporteoferta.idjornada">
				<option ng-repeat="jornada in jornadas" value="{{jornada.id}}">
					{{jornada.nombrejornada}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Departamento</span>
			<select ng-model="reporteoferta.iddepartamento">
				<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">
					{{departamento.descripciondepartamento}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Municipio</span>
			<select ng-model="reporteoferta.idmunicipio">
				<option ng-repeat="municipio in municipios | filter : { iddepartamento: reporteoferta.iddepartamento }" value="{{municipio.id}}">
				{{municipio.descripcionmunicipio}}
			</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<span>Fecha de Postulación</span>
			<input type="date" class="form-control" ng-model="reporteoferta.fechapostulacion" placeholder="Fecha de postulación" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Filtrar" name="enviar" />
		</div>
	</div>
</form>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Empleo</th>
					<th class="text-turquoise">Empresa</th>
					<th class="text-turquoise">Requerimientos</th>
					<th class="text-turquoise">Número de Vacantes</th>
					<th class="text-turquoise">Jornada</th>
					<th class="text-turquoise">Municipio</th>
					<th class="text-turquoise">Sueldo</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="empleo in empleos" 
				ng-click="'empleos/'+empleo.id | go">
					<td>{{empleo.nombreempleo}}</td>
					<td>{{empleo.empresa.nombreempresa}}</td>
					<td>{{empleo.requerimientos}}</td>
					<td>{{empleo.numerovacantes}}</td>
					<td>{{empleo.jornada.nombrejornada}}</td>
					<td>{{empleo.municipio.descripcionmunicipio}}</td>
					<td>Q. {{empleo.sueldo}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>