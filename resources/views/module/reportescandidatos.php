<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Reporte de Candidatos</h4>
		<span>Puedes utilizar los filtros para obtener mejores resultados</span>
	</div>
</div>

<form enctype="multipart/form-data" ng-submit="generateReportCandidatos()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<span>Habilidad</span>
			<select ng-model="reportecandidato.idhabilidad">
				<option ng-repeat="habilidad in habilidades" value="{{habilidad.id}}">
					{{habilidad.deschabilidad}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Departamento</span>
			<select ng-model="reportecandidato.iddepartamento">
				<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">
					{{departamento.descripciondepartamento}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Municipio</span>
			<select ng-model="reportecandidato.idmunicipio">
				<option ng-repeat="municipio in municipios | filter : { iddepartamento: reportecandidato.iddepartamento }" value="{{municipio.id}}">
				{{municipio.descripcionmunicipio}}
			</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-3 col-xs-offset-1">
			<span>Género</span>
			<select ng-model="reportecandidato.genero">
				<option value="0" selected="">Seleccione el Género</option>
				<option value="1">Hombre</option>
				<option value="2">Mujer</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Filtrar" name="enviar" />
		</div>
	</div>
</form>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombres</th>
					<th class="text-turquoise">Direccion</th>
					<th class="text-turquoise">E-Mail</th>
					<th class="text-turquoise">Teléfonos</th>
					<th class="text-turquoise">Fecha de Nacimiento</th>
					<th class="text-turquoise">Municipio</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="candidato in candidatos" 
				ng-click="'candidatos/'+candidato.id | go">
					<td>{{candidato.nombres}} {{candidato.apellidos}}</td>
					<td>{{candidato.direccionresidencia}}</td>
					<td>{{candidato.email}}</td>
					<td>{{candidato.telpersonal}} {{candidato.telcasa}}</td>
					<td>{{candidato.fechanacimiento}}</td>
					<td>{{candidato.idmunicipio}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>