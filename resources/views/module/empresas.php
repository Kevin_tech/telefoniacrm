<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">Empresas registradas</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre de la Empresa</th>
					<th class="text-turquoise">Descripción</th>
					<th class="text-turquoise">E-Mail</th>
					<th class="text-turquoise">Dirección</th>
					<th class="text-turquoise">N.I.T.</th>
					<th class="text-turquoise">Sitio Web</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="empresa in empresas" 
				ng-click="'empresas/'+empresa.id | go">
					<td>{{empresa.nombreempresa}}</td>
					<td>{{empresa.descripcionempresa}}</td>
					<td>{{empresa.email}}</td>
					<td>{{empresa.direccionempresa}}</td>
					<td>{{empresa.nitempresa}}</td>
					<td>{{empresa.websiteempresa}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/empresas/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>