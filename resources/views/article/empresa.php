<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Datos de: {{empresa.nombreempresa}}</h4>
	</div>
</div>

<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.nombreempresa" placeholder="Nombre de la empresa" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="empresa.descripcionempresa" placeholder="Descripción" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.email" placeholder="E-Mail" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="empresa.direccionempresa" placeholder="Dirección" required />
		</div>
		<div class="col-xs-3">
			<input type="text" class="form-control" ng-model="empresa.nitempresa" placeholder="N.I.T." required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empresa.websiteempresa" placeholder="Sitio Web" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Modificar" name="enviar" />
		</div>
	</div>
</form>

<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Sectores de la empresa: {{empresa.nombreempresa}}</h4>
	</div>
</div>

<div class="row row-gutter">
	<div class="col-xs-4 col-xs-offset-1">
		<select ng-model="empresasector.idsector">
			<option ng-repeat="sector in sectores" value="{{sector.id}}">
				{{sector.nombresectoremp}}
			</option>
		</select>
	</div>
	<div class="col-xs-5">
		<a class="btn btn-lg btn-success" ng-click="addSector()">Agregar</a>
	</div>
</div>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Sector</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="empresasector in empresasectores | filter : { idempresa: empresa.id }">
					<td>{{empresasector.sector.nombresectoremp}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>