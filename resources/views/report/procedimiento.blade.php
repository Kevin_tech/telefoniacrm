<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">

		html,body
		{
			font-family: Arial;
			font-size: 16px;
			padding-top: 180px;
			padding-bottom: 45px;
		}

		table
		{
			border-collapse: collapse;
		}

		.actividades
		{
			border: 1px;
			margin: 1em 0;
		}

		.actividades td
		{
			padding: 0.25em 0.5em;
			text-align: center;
			text-transform:initial;
		}

		.actividades thead th
		{
			font-weight: bold;
		}

		.actividades .justify
		{
			text-align: justify;
		}

		.comision
		{
			font-size: 20px;
			font-weight: bold;
			padding-left: 200px;
			position: fixed;
			text-align: center;
			top: 25px;
			width: 100%;
		}

		.footer
		{
			bottom: 0;
			height: 60px;
			position: fixed;
			right: 0;
			text-align: center;
			width: 100%;
		}

		.footer img
		{
			width: 100%;
		}

		.list li
		{
			text-transform: capitalize;
		}

		.itinfom
		{
			bottom: 50px;
			font-size: 12px;
			height: 30px;
			position: fixed;
			right: 0;
			text-align: right;
			width: 100px;
		}

		.itinfom img
		{
			height: 100%;
		}

		.logo
		{
			height: 150px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.pagenum:before
		{
			content: counter(page);
		}

		.page-break
		{
			page-break-after: always;
		}

		.resumen
		{
			border: 1px solid black;
			font-size: 11px;
			margin-bottom: 1em;
			padding: 1em;
			position: absolute;
			right: 0;
			top: 0;
			width: 220px;
		}

		.resumen h1
		{
			background: white;
			font-size: 12px;
			margin: -1.5em auto 0;
			padding: 0;
			text-align: center;
			width: 110px;
		}
	</style>
</head>
<body>

	<div class="logo">
		<img src="{{public_path()}}/img/membrete-logo.jpg" />
	</div>

	<div class="comision">
		COMISION TÉCNICA - INFOM
	</div>

	<div class="footer">
		<img src="{{public_path()}}/img/membrete-pie.jpg" />
	</div>

	<div class="itinfom">
		<img src="{{public_path()}}/img/it.png" />
		<br/>Página: <span class="pagenum"></span> / 
		<script type="text/php">
			$text = '{PAGE_COUNT}';
			$font = Font_Metrics::get_font("arial");
			$pdf->page_text(580, 721, $text, $font, 8);
		</script>
	</div>

	<ol>
		<li>
			<b>Unidad Administrativa:</b>
			<b>{{ucfirst(strtolower($data->unidad))}}</b>
		</li>
		<li>
			<b>Titulo del Procedimiento:</b>
			<b>{{ucfirst(strtolower($data->titulo))}}</b>
		</li>
		<li>
			<b>Definición del Procedimiento.</b>
		</li>
	</ol>

	<p>{{ucfirst(strtolower($data->definicion))}}</p>

	<b>Objetivos del Procedimiento.</b>
	<ul class="list">{!! $data->objetivos !!}</ul>

	<b>Normas del Procedimiento.</b>
	<ol class="list">{!! $data->normas !!}</ol>

	<div class="page-break"></div>

	<!-- LISTADO DE ACTIVIDADES -->

	<?php
		$unidad_inicia 	= "";
		$unidad_fin 	= "";
		if( count( $data->actividad ) > 0 )
		{
			$unidad_inicia = $unidad_fin = $data->actividad[0]->unidad;
		}

		foreach ($data->actividad as $key => $value)
		{
			if( trim($value->unidad) != "" ) 
			{
				$unidad_fin = $value->unidad;
			}
		}
	?>

	<table class="actividades" border="1" >
		<thead>
			<tr>
				<th colspan="2">Hojas:</th> 
				<th colspan="2">No. de Formas u otras clasificaciones: {{ucfirst(strtolower($data->titulo))}}</th>
			</tr>
			<tr>
				<th colspan="2">Inicia: {{ucfirst(strtolower($unidad_inicia))}}</th> 
				<th colspan="2">Termina: {{ucfirst(strtolower($unidad_fin))}}</th>
			</tr>
			<tr>
				<th>Unidad <br/> Responsable</th> 
				<th>Puesto <br/> Responsable</th>
				<th>Paso <br/> No.</th> 
				<th>Actividad</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data->actividad as $actividad)
				<tr>
					<td>{{ucfirst(strtolower($actividad->unidad))}}</td>
					<td>{{ucfirst(strtolower($actividad->puesto))}}</td>
					<td>{{$actividad->paso}}</td>
					<td class="justify">{{ucfirst(strtolower($actividad->actividad))}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>