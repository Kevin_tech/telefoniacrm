<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">

		html,body
		{
			font-family: Arial;
			font-size: 16px;
			padding-top: 180px;
			padding-bottom: 45px;
		}

		table
		{
			border-collapse: collapse;
		}

		.avatar, .cheque
		{
			border: 4px solid black;
			position: absolute;
			right: 10px;
			width: 220px;
		}

		.cheque
		{
			top: 375px;
		}

		.comision
		{
			font-size: 20px;
			font-weight: bold;
			padding-left: 200px;
			position: fixed;
			text-align: center;
			top: 25px;
			width: 100%;
		}

		.footer
		{
			bottom: 0;
			height: 60px;
			position: fixed;
			right: 0;
			text-align: center;
			width: 100%;
		}

		.footer img
		{
			width: 100%;
		}

		.itinfom
		{
			bottom: 50px;
			font-size: 12px;
			height: 30px;
			position: fixed;
			right: 0;
			text-align: right;
			width: 100px;
		}

		.itinfom img
		{
			height: 100%;
		}

		.leyenda
		{
			padding-top: 35px;
		}

		.list
		{
			font-size: 18px;
			margin: 0;
			padding: 0;
		}

		.list label
		{
			display: block;
			font-size: 18px;
			padding: 3px 0;
			text-align: center;
			width: 400px;
		}

		.list .sub
		{
			display: block;
			font-size: 14px;
			text-align: center;
			width: 400px;
		}

		.logo
		{
			height: 150px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.pagenum:before
		{
			content: counter(page);
		}

		.page-break
		{
			page-break-after: always;
		}


		.signatures
		{
			width: 100%;
			position: relative;
		}

		.signatures > div
		{
			border-top: 1px solid black;
			font-weight: bold;
			text-align: center;
			margin-top: 50px;
			position: absolute;
			width: 220px;
		}

		.signatures .center
		{
			left: 50%;
			margin-left: -150px;
			top: 100px;
			width: 300px;
		}

		.signatures .left
		{
			left: 1em;
		}

		.signatures .right
		{
			right: 1em;
		}
	</style>
</head>
<body>

	<div class="logo">
		<img src="{{public_path()}}/img/membrete-logo.jpg" />
	</div>

	<div class="comision">
		CONSTANCIA DE PAGO
	</div>

	<div class="footer">
		<img src="{{public_path()}}/img/membrete-pie.jpg" />
	</div>

	<div class="itinfom">
		<img src="{{public_path()}}/img/it.png" />
		<br/>Página: <span class="pagenum"></span> / 
		<script type="text/php">
			$text = '{PAGE_COUNT}';
			$font = Font_Metrics::get_font("arial");
			$pdf->page_text(580, 721, $text, $font, 8);
		</script>
	</div>

	<img src="{{$data->src_foto}}" class="avatar" />
	<img class="cheque" src="{{$data->src_cheque}}" />

	<p class="list">
		<b>Nombres y apellidos: </b>
			{{ucfirst(strtolower($data->nombre_1))}} {{ucfirst(strtolower($data->nombre_2))}} {{ucfirst(strtolower($data->nombre_3))}}
			{{ucfirst(strtolower($data->apellido_1))}} {{ucfirst(strtolower($data->apellido_2))}} {{ucfirst(strtolower($data->apellido_3))}}
		<br/><br/>
		
		<b>Número de DPI: </b>
			{{$data->numero_dpi}}
		<br/><br/>
		
		<b>Sexo: </b>
			@if($data->sexo==1) Masculino @else Femenino @endif
		<br/><br/>
		
		<b>Renglon: </b>
			{{ucfirst(strtolower($data->renglon->nombre))}}
		<br/><br/>
		
		<b>Unidad: </b>
			{{ucfirst(strtolower($data->unidad->nombre))}}
		<br/><br/>
		
		<b>Puesto que ocupa: </b>
			{{ucfirst(strtolower($data->puesto))}}
		<br/><br/>
		
		<b>Datos del pago: </b>
		<ul>
			<li>
				<b>Número de referencia del cheque:</b>
				{{ucfirst(strtolower($data->numero_cheque))}}
			</li>
			<li>
				<b>Monto:</b>
				Q. {{$data->monto}}
			</li>
		</ul>
	</p>

	<p class="leyenda">
		Quedando de esta manera terminada con carácter definitivo los servicios prestados, según finiquito emitido a favor del Instituto de Fomento Municipal - INFOM - con fecha 31 de marzo del 2016 firmado por el interesado.
	</p>

	<div class="signatures">
		<div class="center">
			{{ucfirst(strtolower($data->puesto))}}<br/>
			{{ucfirst(strtolower($data->nombre_1))}} {{ucfirst(strtolower($data->nombre_2))}} {{ucfirst(strtolower($data->nombre_3))}}
			{{ucfirst(strtolower($data->apellido_1))}} {{ucfirst(strtolower($data->apellido_2))}} {{ucfirst(strtolower($data->apellido_3))}}
			<br/>
			{{$data->numero_dpi}}
		</div>
	</div>

</body>
</html>