<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Habilidad extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatHabilidades';
    protected $dates = ['deleted_at'];

    /*
    ** TODO:constraint for candidatohabilidad created by this
    public function candidatohabilidad()
    {
        return $this->hasMany("App\CandidatoHabilidad", "idhabilidad");
    }
    */
}
