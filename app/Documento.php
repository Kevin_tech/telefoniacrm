<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
    //
    use SoftDeletes;

    protected $table = 'documento';
    protected $dates = ['deleted_at'];

    public function asignacion()
    {
        return $this->hasMany("App\DocumentoAsignacion", "id_documento")->orderBy("id", "asc");
    }

    /*
    public function asunto()
    {
        return $this->belongsTo("App\Asunto", "id_asunto");
    }
    */

    public function estado()
    {
        return $this->belongsTo("App\Estado", "id_estado");
    }

    public function municipalidad()
    {
        return $this->belongsTo("App\Municipio", "id_municipalidad");
    }

    public function observacion()
    {
        return $this->hasMany("App\DocumentoObservacion", "id_documento")->orderBy("created_at", "asc");
    }

    public function usuario_creo()
    {
        return $this->belongsTo("App\User", "id_usuario_creo");
    }
}
