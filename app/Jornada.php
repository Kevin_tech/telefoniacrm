<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jornada extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatJornada';
    protected $dates = ['deleted_at'];
}
