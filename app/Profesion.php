<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profesion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatProfesion';
    protected $dates = ['deleted_at'];
}
