<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPostulacion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatEstadoPostulacion';
    protected $dates = ['deleted_at'];
}
