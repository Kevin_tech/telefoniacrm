<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoContrato extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatTipoContrato';
    protected $dates = ['deleted_at'];
}
