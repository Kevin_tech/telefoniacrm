<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmpresaSector extends Model
{
    //
    use SoftDeletes;

    protected $table = 'empresasector';
    protected $dates = ['deleted_at'];

    public function sector()
    {
        return $this->belongsTo("App\Sector", "idsector");
    }
}
