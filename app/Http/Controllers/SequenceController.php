<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Sequence;

class SequenceController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth");
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	DB::beginTransaction();
    	$record = Sequence::find( $id );

    	if( $record->delete() )
    	{
    		DB::commit();
    		$response = response()->json([
				"msg"		=> "Record deleted",
				"id"		=> $id
			], 200);
    	}
    	else
    	{
    		DB::rollBack();
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Begin transaction and get MAX for sequence!
    	DB::beginTransaction();
    	$maxRecord 	= Sequence::max("sequence");

    	if( $maxRecord )
    		$nextVal 	= $maxRecord +1;
		else
			$nextVal	= 1;

    	$record = new Sequence();
    	$record->sequence 	    = $nextVal;
        $record->sub_sequence   = 1;

    	if( $record->save() )
    	{
    		DB::commit();
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		DB::rollBack();
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
    ** THIS METHOD INSERT NEW RECORD IN THE DATABASE (SUB-SEQUENCE)
    */
    public function subsequence( Request $request )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "correlativo" => "required|exists:documento,correlativo",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Begin transaction and get MAX for sequence!
        DB::beginTransaction();
        $sequence = intval( substr($request->correlativo, 5) );
        $maxRecord = Sequence::where("sequence", $sequence)->max("sub_sequence");

        if( $maxRecord )
            $nextVal    = $maxRecord +1;
        else
            $nextVal    = 1;

        $record                 = new Sequence();
        $record->sequence       = $sequence;
        $record->sub_sequence   = $nextVal;

        if( $record->save() )
        {
            DB::commit();
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            DB::rollBack();
            $response = response()->json([
                "msg"       => "Error",
                "record"    => Array()
            ], 400);
        }

        return $response;
    }
}
