<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Empleo;

class EmpleoController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = Empleo::find( $id );

    	if( $record )
    	{
    		$record->deleted_at	= time();
    		$record->save();

    		if( $record->trashed() )
    		{
    			$response = response()->json([
					"msg"		=> "Record deleted",
					"id"		=> $id
				], 200);
    		}
    		else
    		{
    			$response = response()->json([
					"msg"		=> "Error",
					"id"		=> $id
				], 400);
    		}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index( Request $request )
    {
        $records = Empleo::with("empresa","jornada","municipio");

        if( $request->idjornada )
            $records = $records->where("idjornada", $request->idjornada);
        
        if( $request->idmunicipio )
            $records = $records->where("idmunicipio", $request->idmunicipio);

        if( $request->fechapostulacion )
            $records = $records->where("fechapostulacion", $request->fechapostulacion);

        $records = $records->get();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = Empleo::with("empresa","jornada","municipio")->find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"nombreempleo" => "required|max:250",
            "descripcionempleo" => "required|max:250",
            "requerimientos" => "required|max:250",
            "numerovacantes" => "required|integer",
            "sueldo" => "required",
            "idempresa" => "integer|exists:empresa,id",
            "idjornada" => "integer|exists:catjornada,id",
            "idmunicipio" => "integer|exists:catmunicipio,id",
            "fechacontratacion" => "required|date",
            "fechapostulacion" => "required|date",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new Empleo();
    	$record->nombreempleo = $request->nombreempleo;
        $record->descripcionempleo = $request->descripcionempleo;
        $record->requerimientos = $request->requerimientos;
        $record->numerovacantes = $request->numerovacantes;
        $record->sueldo = $request->sueldo;
        $record->idempresa = $request->idempresa;
        $record->idjornada = $request->idjornada;
        $record->idmunicipio = $request->idmunicipio;
        $record->fechacontratacion = $request->fechacontratacion;
        $record->fechapostulacion = $request->fechapostulacion;
    	
    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"nombreempleo" => "required|max:250",
            "descripcionempleo" => "required|max:250",
            "requerimientos" => "required|max:250",
            "numerovacantes" => "required|integer",
            "sueldo" => "required",
            "idempresa" => "integer|exists:empresa,id",
            "idjornada" => "integer|exists:catjornada,id",
            "idmunicipio" => "integer|exists:catmunicipio,id",
            "fechacontratacion" => "required|date",
            "fechapostulacion" => "required|date",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = Empleo::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->nombreempleo = $request->nombreempleo;
            $record->descripcionempleo = $request->descripcionempleo;
            $record->requerimientos = $request->requerimientos;
            $record->numerovacantes = $request->numerovacantes;
            $record->sueldo = $request->sueldo;
            $record->idempresa = $request->idempresa;
            $record->idjornada = $request->idjornada;
            $record->idmunicipio = $request->idmunicipio;
            $record->fechacontratacion = $request->fechacontratacion;
            $record->fechapostulacion = $request->fechapostulacion;
	    	
	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
