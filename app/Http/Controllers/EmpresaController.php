<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Empresa;

class EmpresaController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = Empresa::with("empleoempresa")->find( $id );

    	if( $record )
    	{
    		if( count( $record->empleoempresa ) > 0 )
    		{
    			$response = response()->json([
					"msg"		=> "Error, foreign key",
					"id"		=> $id
				], 409);
    		}
    		else
    		{
	    		$record->deleted_at	= time();
	    		$record->save();

	    		if( $record->trashed() )
	    		{
	    			$response = response()->json([
						"msg"		=> "Record deleted",
						"id"		=> $id
					], 200);
	    		}
	    		else
	    		{
	    			$response = response()->json([
						"msg"		=> "Error",
						"id"		=> $id
					], 400);
	    		}
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	$records = Empresa::all();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = Empresa::find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"nombreempresa" => "required|max:250",
            "descripcionempresa" => "required|max:250",
            "email" => "required|max:250",
            "direccionempresa" => "required|max:250",
            "nitempresa" => "required|min:8",
            "websiteempresa" => "max:250",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new Empresa();
        $record->idusuario = 1;
    	$record->nombreempresa = $request->nombreempresa;
        $record->descripcionempresa = $request->descripcionempresa;
        $record->email = $request->email;
        $record->direccionempresa = $request->direccionempresa;
        $record->nitempresa = $request->nitempresa;
        $record->websiteempresa = $request->websiteempresa;
    	
    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"nombreempresa" => "required|max:250",
            "descripcionempresa" => "required|max:250",
            "email" => "required|max:250",
            "direccionempresa" => "required|max:250",
            "nitempresa" => "required|min:8",
            "websiteempresa" => "max:250",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = Empresa::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->nombreempresa = $request->nombreempresa;
            $record->descripcionempresa = $request->descripcionempresa;
            $record->email = $request->email;
            $record->direccionempresa = $request->direccionempresa;
            $record->nitempresa = $request->nitempresa;
            $record->websiteempresa = $request->websiteempresa;
	    	
	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
