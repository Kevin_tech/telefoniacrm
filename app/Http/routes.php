<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get("/", function () {
    return view("login");
});

Route::get("/app", function () {
    return view("index");
});

Route::get("/login", function () {
    return view("login");
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(["middleware" => ["web"]], function () {
    //
});

Route::group(["prefix" => "api"], function()
{
	// Authentication
	Route::get("authenticate", "AuthenticateController@getAuthenticatedUser");
	Route::post("authenticate/login", "AuthenticateController@authenticate");

	// Asunto
	Route::resource("asunto", "AsuntoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	Route::get("documento/{documento}/imagen", "DocumentoController@showImage");

	// Partida Presupuestaria
	Route::resource("partidapresupuestaria", "PartidaPresupuestariaController",
					["only" => ["index"]]);

	// Password User
	Route::post("password", "AuthenticateController@updateUserPassword");
	Route::get("password/{usuario}", "AuthenticateController@resetUserPassword");
	Route::put("password/{usuario}", "AuthenticateController@updateUserPasswordAdmin");

	// Transactionals
	Route::resource("candidato", "CandidatoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("candidatohabilidad", "CandidatoHabilidadController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("empleo", "EmpleoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("empresa", "EmpresaController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("empresasector", "EmpresaSectorController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Catalogs
	Route::resource("areaoferta", "AreaOfertaController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("departamento", "DepartamentoController",
					["only" => ["index"]]);
	Route::resource("estadopostulacion", "EstadoPostulacionController",
					["only" => ["index"]]);
	Route::resource("habilidad", "HabilidadController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("institucioneducativa", "InstitucionEducativaController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("jornada", "JornadaController",
					["only" => ["index"]]);
	Route::resource("municipio", "MunicipioController",
					["only" => ["index"]]);
	Route::resource("pais", "PaisController",
					["only" => ["index"]]);
	Route::resource("profesion", "ProfesionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("sector", "SectorController",
					["only" => ["index"]]);
	Route::resource("tipocontrato", "TipoContratoController",
					["only" => ["index"]]);
	Route::resource("tipoempresa", "TipoEmpresaController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Usuario
	Route::resource("usuario", "AuthenticateController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
});

Route::group(["prefix" => "article"], function()
{
	Route::get("candidato", function () {
		return view("article.candidato");
	});

	Route::get("empleo", function () {
		return view("article.empleo");
	});

	Route::get("empresa", function () {
		return view("article.empresa");
	});

	Route::get("usuario", function () {
		return view("article.usuario");
	});
});

Route::group(["prefix" => "form"], function()
{
	Route::get("areaoferta", function () {
		return view("form.areaoferta");
	});

	Route::get("candidato", function () {
		return view("form.candidato");
	});

	Route::get("empleo", function () {
		return view("form.empleo");
	});

	Route::get("empresa", function () {
		return view("form.empresa");
	});

	Route::get("habilidad", function () {
		return view("form.habilidad");
	});

	Route::get("institucion", function () {
		return view("form.institucion");
	});

	Route::get("tipoempresa", function () {
		return view("form.tipoempresa");
	});

	Route::get("profesion", function () {
		return view("form.profesion");
	});
});

Route::group(["prefix" => "list"], function()
{
	Route::get("usuario", function () {
		return view("list.usuario");
	});
});

Route::group(["prefix" => "module"], function()
{
	Route::get("areasoferta", function () {
		return view("module.areasoferta");
	});

	Route::get("candidatos", function () {
		return view("module.candidatos");
	});

	Route::get("dashboard", function () {
		return view("module.dashboard");
	});

	Route::get("empleos", function () {
		return view("module.empleos");
	});

	Route::get("empleos", function () {
		return view("module.empleos");
	});

	Route::get("empresas", function () {
		return view("module.empresas");
	});

	Route::get("habilidades", function () {
		return view("module.habilidades");
	});

	Route::get("header", function () {
		return view("module.header");
	});

	Route::get("instituciones", function () {
		return view("module.instituciones");
	});

	Route::get("logout", function () {
		return view("module.logout");
	});

	Route::get("tiposempresa", function () {
		return view("module.tiposempresa");
	});

	Route::get("profesiones", function () {
		return view("module.profesiones");
	});

	Route::get("reportescandidatos", function () {
		return view("module.reportescandidatos");
	});

	Route::get("reportesofertas", function () {
		return view("module.reportesofertas");
	});
});

Route::group(["prefix" => "report"], function()
{
	Route::get("/operaciones", "ReporteController@reporteOperaciones");

	Route::get("/procedimiento", "ReporteController@reporteProcedimiento");

	Route::get("/pagocheque", "ReporteController@reportePagoCheque");
});