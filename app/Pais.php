<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatPais';
    protected $dates = ['deleted_at'];
}
