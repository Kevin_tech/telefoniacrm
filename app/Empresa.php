<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
    //
    use SoftDeletes;

    protected $table = 'Empresa';
    protected $dates = ['deleted_at'];

    /*
    ** TODO:constraint for empleoempresa created by this
    public function empleoempresa()
    {
        return $this->hasMany("App\EmpleoEmpresa", "idempresa");
    }
    */

    /*
    ** TODO:constraint for sectorempresa created by this
    public function sectorempresa()
    {
        return $this->hasMany("App\SectorEmpresa", "idempresa");
    }
    */
}
