<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaOferta extends Model
{
    //
    use SoftDeletes;

    protected $table = 'sector';
    protected $dates = ['deleted_at'];
}
