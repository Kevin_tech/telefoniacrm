<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoEmpresa extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatTipoEmpresa';
    protected $dates = ['deleted_at'];
}
