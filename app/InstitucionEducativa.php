<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstitucionEducativa extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatInstitucionEducativa';
    protected $dates = ['deleted_at'];

    /*
    ** TODO:constraint for candidatoinstitucioneducativa created by this
    public function candidatoinstitucioneducativa()
    {
        return $this->hasMany("App\CandidatoInstitucionEducativa", "idinstitucioneducativa");
    }
    */
}
