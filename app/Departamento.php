<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{
    //
    use SoftDeletes;

    protected $table = 'CatDepartamento';
    protected $dates = ['deleted_at'];
}
