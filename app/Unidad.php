<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidad extends Model
{
    //
	use SoftDeletes;

    protected $table = 'unidad';
    protected $dates = ['deleted_at'];

    public function asunto()
    {
        return $this->hasMany("App\Asunto", "id_unidad");
    }

    public function director()
    {
        return $this->belongsTo("App\User", "id_director");
    }

    public function gerente()
    {
        return $this->belongsTo("App\User", "id_gerente");
    }

    public function institucion()
    {
        return $this->belongsTo("App\Institucion", "id_institucion");
    }

    public function supervisor()
    {
        return $this->belongsTo("App\User", "id_supervisor");
    }

    public function usuario()
    {
        return $this->hasMany("App\User", "id_unidad");
    }
}
