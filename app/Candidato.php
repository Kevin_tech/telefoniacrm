<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidato extends Model
{
    //
    use SoftDeletes;

    protected $table = 'Candidato';
    protected $dates = ['deleted_at'];
}
