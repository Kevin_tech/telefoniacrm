<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleo extends Model
{
    //
    use SoftDeletes;

    protected $table = 'EmpleoEmpresa';
    protected $dates = ['deleted_at'];

    public function empresa()
    {
        return $this->belongsTo("App\Empresa", "idempresa");
    }

    public function jornada()
    {
        return $this->belongsTo("App\Jornada", "idjornada");
    }

    public function municipio()
    {
        return $this->belongsTo("App\Municipio", "idmunicipio");
    }

    /*
    ** TODO:constraint for sectorempresa created by this
    public function sectorempresa()
    {
        return $this->hasMany("App\SectorEmpresa", "idempresa");
    }
    */
}
