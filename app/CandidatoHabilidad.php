<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CandidatoHabilidad extends Model
{
    //
    use SoftDeletes;

    protected $table = 'candidatohabilidad';
    protected $dates = ['deleted_at'];

    public function habilidad()
    {
        return $this->belongsTo("App\Habilidad", "idhabilidad");
    }
}
